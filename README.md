# README
All of the above samples will need Node installed on your machine to run. Get node here: https://nodejs.org/en/

Each project has its own README explaining how to get it running. All samples run on Windows, Linux or OS X (except the **classic-asp-example**).


## Directory

**aspnet-core-example:** A realistic example of Webpack being used in a simple ASP.NET core app. This is Microsoft's own template, initialized using `dotnet new react`.

**classic-asp-example:** An example of Webpack added on top of a traditional ASP.NET MVC 5 app. (Windows only)

**crappy-clock:** The "Crappy Clock" user story, implemented the traditional JS way. An example of life without Webpack (or a tool like Webpack).

**crappy-clock-webpack:** The identical "Crappy Clock" user story, but implemented with Webpack.

**simple-webpack-demo:** The simplest usage of Webpack I could imagine.