# aspnet-core-example
A realistic example of Webpack being used in a simple ASP.NET core app. This is Microsoft's own template, initialized using `dotnet new react`.

1. Install node. (https://nodejs.org/en/)
2. Install dotnet core, version 2.0 or newer (https://www.microsoft.com/net/learn/get-started/)
3. From the project root, open a command line/PowerShell prompt, and execute `npm install`. This will download all the npm packages defined in the package.json file.
4. Launch the .net app using Visual Studio or VS Code. (Or, even, from the command line, with the command `dotnet build` followed by `dotnet run`. You can then open the site in a browser at **http://localhost:5000**).
5. Tinker around!