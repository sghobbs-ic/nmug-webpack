# crappy-clock-webpack
The identical "Crappy Clock" user story from **crappy-clock**, but implemented with Webpack.

To run:

1. Make sure you have node installed.
2. Open a command prompt in the same directory as the *package.json* file.
3. Run the command `npm install`, and then `npm run build`.
4. Open the *index.html* file in a browser.
5. Tinker around!

In this project, you'll have to run `npm run build` each time you make a change in the JavaScript to see those changes in the browser. A more mature Webpack project would be configured to do this automatically as you're developing.