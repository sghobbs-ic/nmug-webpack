import moment from 'moment-timezone';

/**
 * Gets the current time with time zone.
 * @returns {String}
 */
export const getTimeWithTimeZone = () =>
    moment.tz(moment(), moment.tz.guess())
        .format('h:mm:ss a zz');