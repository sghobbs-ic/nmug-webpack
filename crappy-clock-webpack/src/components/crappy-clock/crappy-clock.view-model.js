import ko from 'knockout';
import { getTimeWithTimeZone, bongAnnoyingly } from 'utils';

/**
 * ViewModel for <crappy-clock> knockout component.
 */
export default class CrappyClockViewModel{
    constructor(){
        this.currentTime = ko.observable('Loading...');
        this.onBong = this.onBong.bind();
        this.refreshTime = this.refreshTime.bind(this);

        setInterval(this.refreshTime, 1000);
    }

    refreshTime(){
        this.currentTime(getTimeWithTimeZone());
    }

    onBong(){
        bongAnnoyingly();
    }
}