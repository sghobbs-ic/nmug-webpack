import CrappyClockViewModel from './crappy-clock.view-model';
import CrappyClockTemplate from './crappy-clock.template.html';

/**
 * A configured Knockout component for <crappy-clock>
 */
export default {
    viewModel: CrappyClockViewModel,
    template: CrappyClockTemplate
};