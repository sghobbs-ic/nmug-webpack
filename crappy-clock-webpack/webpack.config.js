const webpack = require('webpack');
const path = require('path');

module.exports = {
    context: __dirname,
    devtool: 'source-map',
    entry: './src/index.js', // <---- Start with this file
    output: {
        filename: 'bundle.js', // <---- Put result here
        path: path.resolve(__dirname)
    },
    resolve: {
        modules: [
           '/', 'node_modules'
        ],
        alias: {
            utils: path.resolve(__dirname + '/src/utils/index')
        }
    },
    module: {
        rules: [
              {
                 test: /\.js$/,
                 exclude: /node_modules/,
                 use: [
                    {
                       loader: 'babel-loader',
                       options: {
                          presets:['es2015']
                       }
                    }
                 ]
              },
              { test: /\.html$/, use: 'raw-loader' }
           ]
     }
};