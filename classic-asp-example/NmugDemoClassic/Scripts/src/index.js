import ko from 'knockout';
import CrappyClock from './components/crappy-clock';

ko.components.register('crappy-clock', CrappyClock);

ko.applyBindings();

if (module.hot) {
   module.hot.accept();
   module.hot.dispose(() => ko.cleanNode(document.body));
}