import { getTimeWithTimeZone } from 'utils';

/**
* Irritates the user with the current time.
*/
export const bongAnnoyingly = () =>
   alert(`BONG. The time is ${getTimeWithTimeZone()}!`);