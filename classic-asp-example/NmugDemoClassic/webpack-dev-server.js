﻿/**
 * A node.js script that instantiates a WebpackDevServer instance
 * at http://localhost:3000, with CORS set up for reloads from
 * the web app. (localhost:50077)
 */

const webpack = require('webpack');
const WebpackDevServer = require('webpack-dev-server');
const config = require('./webpack.dev.config.js');

const port = 3000,
   options = {
      publicPath         : config.output.publicPath,
      hot                : true,
      historyApiFallback : true,
      host               : 'localhost',
      port               : port,
      headers: {
         //Must match the Visual Studio IIS Express debug port
         'Access-Control-Allow-Origin': 'http://localhost:50077'
      }
   }

WebpackDevServer.addDevServerEntrypoints(config, options);
const compiler = webpack(config);

new WebpackDevServer(compiler, options)
   .listen(port, '0.0.0.0', (err,result) => {
      if (err) {
         return console.log(err);
      }

      console.log(`Listening at http://localhost:${port}/`);
   });