﻿const webpack = require('webpack');
const path = require('path');

module.exports = {
   context: path.resolve(__dirname, 'scripts/src'),
   devtool: 'source-map',
   entry: [
      'babel-polyfill',
      './index.js'
   ],
   //Generates /Scripts/bundle.js shim that will be the hot dev server
   output: {
      path: path.resolve(__dirname, 'src/bundles'),
      filename: 'bundle.js',
      publicPath: 'http://localhost:3000/bundles/'
   },
   resolve: {
      modules: [
         '/', 'node_modules'
      ],
      alias: {
         utils: path.resolve(__dirname, 'scripts/src/utils/index')
      }
   },
   module: {
      rules: [
         {
            test: /\.js$/,
            exclude: /node_modules/,
            use: [
               {
                  loader: 'babel-loader',
                  options: {
                     presets: ['es2015']
                  }
               }
            ]
         },
         { test: /\.html$/, use: 'raw-loader' }
      ]
   },
   plugins: [
      //Needed for React hot-module-replacement to work:
      new webpack.NamedModulesPlugin(),
      new webpack.HotModuleReplacementPlugin()
   ]
};