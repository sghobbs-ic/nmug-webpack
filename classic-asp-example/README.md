# aspnet-core-example
An example of Webpack added on top of a traditional ASP.NET MVC 5 app. (Windows only) This site was created from a fresh, boring ol' Visual Studio template. An npm *project.json* and *webpack.dev.config.js* were added to the project. In this project, Webpack-managed */Scripts/src* JavaScript is being used side-by-side with the other JavaScript resources in */Scripts*. Over time, you would gradually refactor JS functionality to the Webpack-managed directory.

**Note:** Since Visual Studio doesn't natively support running Webpack alongside an ASP.NET MVC 5 site, you'll need to run the server-side app in Visual Studio while also launching a Webpack dev server from the command line. For my day-to-day development on a project like this, I usually have Visual Studio open and running for C# development, and then I develop the JS client in VS Code.

1. Make sure you have node installed. (https://nodejs.org/en/)
2. Make sure you have a recent-ish version of Visual Studio.
3. Open *NmugDemoClassic.sln* in Visual Studio.
4. Open a command line in the same directory as the *package.json* file. (The same directory as the *NmugDemoClassic.csproj* file.)
5. Execute the comman `npm install`. This downloads the packages references in the *package.json*.
6. Execute the command `npm run dev`. This starts up a mini-server at http://localhost:3000, which dynamically rebundles and refreshes your JavaScript in the browser as you write it.
7. Run the site in Visual Studio. In the *web.config*, the ASP site redirects requests for */bundle.js* to http://localhost:3000 (the webpack mini-server), which makes the whole thing work.
8. Tinker around!

**Also Note:** As a demo, I only set up an example of a development Webpack bundle. For production, you wouldn't run the mini-server (obviously), but you would actually build a tangible bundle.js that would be published with the rest of the site.