const path = require('path');

module.exports = {
    entry: './src/index.js', // <---- Start with this file
    output: {
        filename: 'bundle.js', // <---- Put result here
        path: path.resolve(__dirname)
    }
};