/**
 * Gets the current time with time zone.
 * @returns {String}
 */
function getTimeWithTimeZone(){
    return moment.tz(moment(), moment.tz.guess()).format('h:mm:ss a zz');
}